# simple fast-forward merge by default
```
# make the first change
> git checkout -b first
> touch one
> git add one
> git commit -m "Added one."

# branch off of first and make the second change
> git checkout -b second
> touch two
> git add two
> git commit -m "Added two."

# fast-forward merge back onto first
> git checkout first
> git merge second
```

# force merge commit instead of fast-forward merge
```
# make the first change
> git checkout -b first
> touch one
> git add one
> git commit -m "Added one."

# branch off of first and make the second change
> git checkout -b second
> touch two
> git add two
> git commit -m "Added two."

# fast-forward merge back onto first
> git checkout first
> git merge second --no-ff --no-edit
``` 

# resolve merge conflict with merge tool
```
# create the base file
> git checkout master
> echo text >> file.txt
> git add file.txt
> git commit -m "Added file.txt."

# create the first change on a new branch off of master
> git checkout -b first
> echo one >> file.txt
> git add file.txt
> git commit -m "Added one."

# create the second change on a new branch off of master
> git checkout master
> git checkout -b second
> echo two >> file.txt
> git add file.txt
> git commit -m "Added two."

# go back to master and fast-forward merge from first
> git checkout master
> git merge first

# trigger merge conflict by merging from second
> git merge second

# run merge tool
> git mergetool

# resolve conflicts in merge tool and then exit

# add the resolved files
> git add file.txt

# commit using the default merge message
> git commit --no-edit
```
