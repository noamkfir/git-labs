# stash - push and pop
```
> echo one >> file.txt
> git add file.txt
> git commit -m "One."

> echo three >> file.txt
> git add file.txt
> git stash

> echo two >> file.txt
> git add file.txt
> git commit -m "Two."

> git stash pop
> git mergetool
# result should have 'one', 'two' and 'three'
> git commit -m "Three."
```

# stash - save, apply and drop
```
> echo one >> file.txt
> git add file.txt
> git commit -m "One."

> echo two >> file.txt
> git add file.txt
> git stash save "stash for two"

> git stash apply stash@{0}
> git add .
> git commit -m "Two."

> git stash drop stash@{0}
```
