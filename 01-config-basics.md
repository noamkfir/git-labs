# list all config settings
```
> git config --list  # or -l
```

# list only global config settings
```
> git config -l --global
```

# list only local config settings
```
> git config -l --global
```

# set name locally
```
> git config --local user.name "my name"
```

# set email locally
```
> git config --local user.email "me@email.com"
```
