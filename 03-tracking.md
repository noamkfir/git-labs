# track remote for current branch
```
> git checkout wip
> git branch --set-upstream remote/wip
```

# track remote for different branch
```
> git checkout master
> git branch --set-upstream remote/wip wip
```
