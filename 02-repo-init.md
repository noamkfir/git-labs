# create repo in current directory
```
> mkdir repo
> cd repo
> git init
```

# create repo in new directory
```
> git init repo
> cd repo
```

# add https remote
```
> git remote add https-remote https://github.com/git/git.git
``` 

#add ssh remote
```
> git remote add ssh-remote git@github.com:git/git.git 
```

# clone repo
```
> git clone https://path/to/repo.git
> cd repo
```

# clone repo to custom directory
```
> git clone https://path/to/repo.git custom-dir
> cd custom-dir
```

# create a bare repo
```
> mkdir repo
> cd repo
> git init --bare
```

# create a bare repo in new directory
```
> git init --bare repo
> cd repo
```
