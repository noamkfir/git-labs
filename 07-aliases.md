# add some useful aliases
```
> git config --global alias.co checkout
> git config --global alias.br branch
> git config --global alias.ci commit -v
> git config --global alias.st status
> git config --global alias.hist log --graph --oneline --decorate=short
> git config --global alias.last log -1 HEAD
```
