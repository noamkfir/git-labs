# create branch from detached head
```
> git checkout master
> echo one >> file.txt
> git add file.txt
> git commit -m "One."

> echo two >> file.txt
> git add file.txt
> git commit -m "Two."

> git checkout HEAD~
> git checkout -b reattached
> echo one point five >> file.txt
> git add file.txt
> git commit -m "Reattached."
```
