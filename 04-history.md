# show basic history
```
> git log
```

# show terse unformatted history
```
> git log --oneline
```

# show ascii graph next to history
```
> git log --graph
```

# show branches and tags
```
> git log --decorate=short
```

# show branches, tags and refs
```
> git log --decorate=full
```

# combine them
```
> git log --graph --oneline --decorate=short
```
