# rebase with conflict and fast-forward merge
```
# create base file
> git checkout master
> echo one >> file.txt
> git add file.txt
> git commit -m "One."

# make change in wip
> git checkout wip
> echo three >> file.txt
> git add file.txt
> git commit -m "Three."

# make parallel change in master
> git checkout master
> echo two >> file.txt
> git add file.txt
> git commit -m "Two."

# rebase wip onto master
> git checkout wip
> git rebase master
# can also use shorter version: > git rebase master wip

# merge to resolve conflicts
> git mergetool
# resolve conflicts in merge tool
> git rebase --continue
 
# fast-forward merge wip onto master
> git checkout master
> git merge wip
```

# rebase interactively to reorder commits
```
# create base file
> git checkout master
> echo one >> file.txt
> git add file.txt
> git commit -m "One."

# make first change in wip
> git checkout wip
> echo three >> file.txt
> git add file.txt
> git commit -m "Three."

# make second change in wip
> echo two >> file.txt
> git add file.txt
> git commit -m "Two."

# rebase interactively to reorder two and three
> git rebase -i master   # or --interactive
# press 'dd' to cut the first line
# press 'p' to paste it as the second line
# press ':x' to begin the rebase process

# apply the first merge
> git mergetool
# resolve conflicts in merge tool - the result should have 'one' and 'two'
> git rebase --continue
# press ':x' to apply the rebase step

# apply the second merge
> git mergetool
# resolve conflicts in merge tool - the result should have 'one', 'two' and 'three'
> git rebase --continue
# press ':x' to apply the rebase step

# fast-forward merge wip onto master
> git checkout master
> git merge wip
```
