# create annotated tag
```
> echo one >> file.txt
> git add .
> git commit -m "One."

# create the tag
> git tag -a v1.0 -m "Version 1.0"

# show all tags
> git tag

# show info for one tag
> git show v1.0
```
